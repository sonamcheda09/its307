from flask import Flask, render_template, request

import pickle
model = pickle.load(open("mymodel.pkl", "rb"))

app = Flask(__name__)


@app.route("/", methods=['GET', 'POST'])
def main():
    return render_template("home.html")


@app.route('/form', methods=["POST"])
def form():
    Gender = request.form.get("gender1")
    Age = request.form.get("age")
    Debt = request.form.get("debt")
    Married = request.form.get("married")
    BankCustomer = request.form.get("bankcustomer")
    EducationLevel = request.form.get("educationLevel")
    Ethnicity = request.form.get("ethnicity")
    YearEmployed = request.form.get("yearemployed")
    PriorDefault = request.form.get("priordefault")

    Employed = request.form.get("employed")
    CreditScore = request.form.get("creditScore")
    DriversLicense = request.form.get("driversLicense")
    Citizen = request.form.get("citizen")
    ZipCode = request.form.get("zipCode")
    Income = request.form.get("income")

    arr = model.predict([[Gender, Age, Debt, Married, BankCustomer, EducationLevel, Ethnicity,
                        YearEmployed, PriorDefault, Employed, CreditScore, DriversLicense, Citizen, ZipCode, Income]])
    print(arr)

    return render_template("form.html", data=arr)


if __name__ == '__main__':
    #app.debug = True
    app.run(debug=True)
